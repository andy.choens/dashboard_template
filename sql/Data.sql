-- Data.sql
-- Grain: MRN
-- Shows the number of active problems for each patient in the
-- CCP/CCMG EHRs. Only includes patients with a PCP who is a primary
-- care provider. Includes some cols useful for stratifying results.

select 
     emp.RowSourceDSC
    ,emp.NextAppointmentDTS
    ,emp.MRN
    ,emp.LegacyGroupNM
    ,pss.SiteAbbreviationNM
    ,pss.SiteNM
    ,pss.ProviderFullNM
    ,pss.FamilyPracticeFLG
    ,pss.InternalMedicineFLG
    ,pss.PediatricsFLG
    ,pss.SpecialityDSC
    ,count(prb.ProblemID) ActiveProblemsNBR 
from MasterCCP.AllPatients.Empanelment emp
inner join IDEA.ProviderSiteandSpecialty.ProviderSiteandSpecialty pss
on emp.PCPID = pss.ProviderID and emp.LegacyGroupNM = pss.SourceDSC
inner join MasterCCP.AllPatients.Problems prb
on emp.LegacyGroupNM = prb.LegacyGroupNM and emp.PatientID = prb.PatientID
where 1 = 1 
   and prb.ActiveProblemFLG = 1 
   and prb.ICDDiagnosisCD not like 'z%'
   and emp.DeceasedFLG = 'N'
   and emp.PrimaryAccountFLG = 1
   and pss.PrimaryCareFLG = 1
group by
     emp.RowSourceDSC
    ,emp.NextAppointmentDTS
    ,emp.MRN
    ,emp.LegacyGroupNM
    ,pss.SiteAbbreviationNM
    ,pss.SiteNM
    ,pss.ProviderFullNM
    ,pss.FamilyPracticeFLG
    ,pss.InternalMedicineFLG
    ,pss.PediatricsFLG
    ,pss.SpecialityDSC
;
