import: data/Data.feather
report: reports/report.html

data/Data.feather: sql/Data.sql
	Rscript -e 'source("R/from_edw.R"); from_edw("Data")'

reports/report.html: reports/report.Rmd R/setup.R
	Rscript -e 'rmarkdown::render("reports/report.Rmd", knit_root_dir = getwd())'
