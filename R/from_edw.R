#' Imports queries from a SQL file & saves it as a local feather file.
#' @param file_name The SQL file name. Assumes the file is in ~/sql
#'     and ends in ".sql"
#' @param convert_dts Converts all Health Catalyst *DTS column names
#'     to *DT column names.
#' @return Returns success (TRUE) or failure (FALSE).
from_edw <- function(file_name="Data", convert_dts=TRUE){

    library(feather)
    library(lubridate)
    library(readr)
    library(tidyverse)
    ##library(ahconnect)
    ##library(ahconfig)

    source("R/open_con.R")
    source('vars.R')

    ## DOWNLOAD ================================================================
    ## Runs the query, downloads the data, and closes the connection.
    ## If the query returns no rows, this will return a warning.

    message("Downloading: ", file_name)
    sql_file <- paste0("sql/",file_name, ".sql")
    if (file.exists(sql_file)) {
        qry <- read_file(sql_file)
        ## TODO: Add the ability to handle variables.
        ## https://db.rstudio.com/best-practices/run-queries-safely/
    } else {
        stop(paste0("Unable to find sql file: ", sql_file))
    }

    con <- open_con(secrets=db$secrets_file, srv=db$srv)
    Data <- dbGetQuery(con, qry) %>% as_tibble()
    if (nrow(Data) == 0) warning("Your query did not download any data.")
    if (interactive()) str(Data)
    message("Download complete.")
    message("Rows in ", file_name, ": ", nrow(Data))
    dbDisconnect(con)



    ## MUNGE ===================================================================
    ## Cleans up the data.

    ## Converts DTS columns to date fields AND changes the name of the column from
    ## *DTS to *DT. To disable this, set convert_dts to FALSE.
    if (convert_dts) {
        which_dts <- grep("DTS", names(Data))
        for (i in which_dts) {
            Data[,i] <- as.Date(pull(Data,i))
        }
        names(Data)[which_dts] <-   gsub("DTS","DT",names(Data)[which_dts])
    }

    ## TODO: Data Munging.
    ## TODO: Data Validation

    ## Log =========================================================================
    if ("LastLoadDTS" %in% names(Data)) {
        ## If LastLoadDTS is a column, saves the max value.
        if (!is.Date(Data$LastLoadDTS)) {
            Data$LastLoadDTS <- as.Date(Data$LastLoadDTS)
        }
        LastRunDays <- max(Sys.Date() - Data$LastLoadDTS)
    }
    if ("LastLoadDT" %in% names(Data)) {
        ## If LastLoadDT is a column, saves the max value.
        if (!is.Date(Data$LastLoadDT)) {
            Data$LastLoadDT <- as.Date(Data$LastLoadDT)
        }
        LastRunDays <- max(Sys.Date() - Data$LastLoadDT)
    }
    if ("EDWLastModifiedDTS" %in% names(Data)) {
        ## If EDWLastModifiedDTS is a column, saves the max value.
        if (!is.Date(Data$EDWLastModifiedDTS)) {
            Data$EDWLastModifiedDTS <- as.Date(Data$EDWLastModifiedDTS)
        }
        LastRunDays <- max(Sys.Date() - Data$EDWLastModifiedDTS)
    }
    if ("EDWLastModifiedDT" %in% names(Data)) {
        ## If EDWLastModifiedDT is a column, saves the max value.
        if (!is.Date(Data$EDWLastModifiedDT)) {
            Data$EDWLastModifiedDT <- as.Date(Data$EDWLastModifiedDT)
        }
        LastRunDays <- max(Sys.Date() - Data$EDWLastModifiedDT)
    }
    if (exists("LastRunDays")) {
        log <- data.frame(
            Run         = Sys.time(),
            Interactive = interactive(),
            User        = Sys.info()[["user"]],
            Sys         = Sys.info()[["sysname"]],
            Node        = Sys.info()[["nodename"]],
            Srv         = db$srv,
            ##Srv         = cfg[['database']][['server']],
            ##Secrets     = 'ahconfig',
            Rows        = nrow(Data),
            LastRunDays = LastRunDays)
    } else {
        log <- data.frame(
            Run         = Sys.time(),
            Interactive = interactive(),
            User        = Sys.info()[["user"]],
            Sys         = Sys.info()[["sysname"]],
            Node        = Sys.info()[["nodename"]],
            Srv         = db$srv,
            ##Srv         = cfg[['database']][['server']],
            ##Secrets     = 'ahconfig',
            Rows        = nrow(Data))
    }
    log_file <- paste0("data/", file_name, ".log")
    write_csv(x = log, path = log_file, append = TRUE)



    ## SAVE ========================================================================
    if (exists("LastRunDays")) {
        if (LastRunDays > 1) warning("Last Run Days: ", LastRunDays)
        write_feather(x = Data, path = paste0("data/",file_name,".feather"))
    } else {
        write_feather(x = Data, path = paste0("data/",file_name,".feather"))
    }
} ## END from_edw
